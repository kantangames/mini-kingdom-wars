package com.kantangames.minikingdomwars;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.kantan.mkw.application.MiniKingdomWars;

public class Main {
	
	public static void main(String[] args) {
		ApplicationListener listener = new MiniKingdomWars();
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "MiniKingdomWars";
		cfg.vSyncEnabled = true;
		cfg.useGL20 = true;
		cfg.width = 1280;
		cfg.height = 720;
		
		new LwjglApplication(listener, cfg);
	}
}

/* The main class file (Main.java / MainActivity.java) will
 * execute the game starting from here.
 */

package com.kantan.mkw.application;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.kantan.mkw.view.LevelInformation;
import com.kantan.mkw.view.LevelSelect;
import com.kantan.mkw.view.LoadedLevel;
import com.kantan.mkw.view.MainMenu;
import com.kantan.mkw.view.Splash;


public class MiniKingdomWars extends Game {

	// constant used for logging info
	public static final String LOG = MiniKingdomWars.class.getSimpleName();
	
	// Libgdx helper class for logging current FPS
	private FPSLogger fpsLogger;
	private BitmapFont font;
	private SpriteBatch spriteBatch;
	
	public MiniKingdomWars() {
	}
	
	// Public methods for screens
	public LevelInformation getLevelInformationScreen() {
		return new LevelInformation(this);
	}
	
	public LevelSelect getLevelSelectScreen() {
		return new LevelSelect(this);
	}
	
	public LoadedLevel getLoadedLevelScreen() {
		return new LoadedLevel(this);
	}
	
	public MainMenu getMainMenuScreen() {
		return new MainMenu(this);
	}
	
	public Splash getSplashScreen() {
		return new Splash(this);
	}
	
	// Game creation
	public void create() {
		Gdx.app.log(MiniKingdomWars.LOG, "Creating game");
		fpsLogger = new FPSLogger();
		font = new BitmapFont();
		spriteBatch = new SpriteBatch();
		
		// Set screen to this on game start
		setScreen(getSplashScreen());
	}
	
	public void dispose() {
		super.dispose();
		Gdx.app.log(MiniKingdomWars.LOG, "Disposing Game");
	}
	
	public void render() {
		super.render();
		
		spriteBatch.begin();
		font.draw(spriteBatch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, 20);		
		spriteBatch.end();
		
//		fpsLogger.log();
	}
	
	public void resize(int width, int height) {
		Gdx.app.log(MiniKingdomWars.LOG, "Resizing window");
		super.resize(width, height);
	}
	
	public void pause() {
		super.pause();
		Gdx.app.log(MiniKingdomWars.LOG, "Pausing game");
	}
	
	public void resume() {
		super.resume();
		Gdx.app.log(MiniKingdomWars.LOG, "Resuming game");
	}
}

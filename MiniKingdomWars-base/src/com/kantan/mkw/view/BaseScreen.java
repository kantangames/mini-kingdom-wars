package com.kantan.mkw.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import com.kantan.mkw.application.MiniKingdomWars;

public abstract class BaseScreen implements Screen {
	
	protected MiniKingdomWars game;
	protected Stage stage;
	
	private BitmapFont font;
	private SpriteBatch batch;
	private Skin skin;
	private TextureAtlas atlas;
	private Table table;
	
	public BaseScreen(MiniKingdomWars game) {
		this.game = game;
		this.stage = new Stage(0, 0, true);
	}
	
	protected String getName() {
		return getClass().getSimpleName();
	}
	
	
	// Can lazily load these collaborators
	
	public BitmapFont getFont() {
		if(font == null) {
			font = new BitmapFont();
		}
		return font;
	}
	
	public SpriteBatch getBatch() {
		if(batch == null) {
			batch = new SpriteBatch();
		}
		return batch;
	}
	
	// This will be used later once we have a UI atlas pack
	
	//public TextureAtlas getAtlas() {
	//	if(atlas == null) {
	//		atlas = new TextureAtlas(Gdx.files.internal("image-atlases/pages.atlas"));
	//	}
	//	return atlas;
	//}
	
	protected Skin getSkin() {
		if(skin == null) {
			FileHandle skinFile = Gdx.files.internal("ui/uiskin.json");
			skin = new Skin(skinFile);
		}
		return skin;
	}
	
	protected Table getTable() {
		if(table == null) {
			table = new Table(getSkin());
			table.setFillParent(true);
			stage.addActor(table);
		}
		return table;
	}
	
	// Screen implementations:
	@Override
	public void show() {
		Gdx.app.log(MiniKingdomWars.LOG, "Showing screen: " + getName());
		
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void resize(int width, int height) {
		Gdx.app.log(MiniKingdomWars.LOG, "Resizing screen: " + getName() + " to: " + width + "x" + height);
		stage.setViewport(width, height, true);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		stage.act(delta);
		stage.draw();
		
		//Table.drawDebug(stage);
	}
	
	@Override
	public void hide() {
		Gdx.app.log(MiniKingdomWars.LOG, "Hiding screen: " + getName());
		
		dispose();
	}
	
	@Override
	public void pause() {
		Gdx.app.log(MiniKingdomWars.LOG, "Pausing screen: " + getName());
	}
	
	@Override
	public void resume() {
		Gdx.app.log(MiniKingdomWars.LOG, "Resuming screen: " + getName());
		
	}
	
	@Override
	public void dispose() {
		Gdx.app.log(MiniKingdomWars.LOG, "Disposing screen: " + getName());
		stage.dispose();
		if(font != null) font.dispose();
		if(batch != null) batch.dispose();
		if(skin != null) skin.dispose();
		if(atlas != null) atlas.dispose();
	}
	
	
}

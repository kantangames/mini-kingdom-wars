package com.kantan.mkw.view;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.Input;
import com.kantan.mkw.application.MiniKingdomWars;
import com.kantan.mkw.controller.GameController;
import com.kantan.mkw.model.Soldier;


public class LoadedLevel extends BaseScreen implements Observer, GestureListener {
	
	private BitmapFont white;
	private Group group;
	private Image actor;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private Skin skin;
	private TextureAtlas textureAtlas, soldierAtlas;
	private TiledMap map;
	private Window armySetupWindow;

	private float soldierScaling;
	private int numSoldiers;
	private Label soldierTypeLabel;
	private Label numSoldiersLabel;
	private String numSoldiersString = "0";		
	private float rotationSpeed = 0.5f;
	
	// Hoping to use this to keep track of all actors in the screen
	private ArrayList<Image> actorArray = new ArrayList<Image>();
	
	 // for pinch-to-zoom
	 int numberOfFingers = 0;
	 int fingerOnePointer;
	 int fingerTwoPointer;
	 float lastDistance = 0;
	 Vector3 fingerOne = new Vector3();
	 Vector3 fingerTwo = new Vector3();
	
	public LoadedLevel(MiniKingdomWars game) {
		super(game);
	}
	
	@Override
	public void render(float delta) {
    	Gdx.gl.glClearColor(0, 0, 1, 1);
    	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    	handleInput();
    	camera.update();
		renderer.setView(camera);
		renderer.render();	
		
		stage.act(delta);
		stage.draw();
	}
	
	@Override
	public void show() {
		super.show();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 1280, 720);
		Gdx.input.setInputProcessor(stage);
		
		// Texture Atlas initialization and styles setup
		textureAtlas = new TextureAtlas("ui/blue.pack");
		skin = new Skin(textureAtlas);
		white = new BitmapFont(Gdx.files.internal("font/white.fnt"), false);
			
    	// Textbutton style 'font' is REQUIRED
    	TextButtonStyle textButtonStyle = new TextButtonStyle();
    	textButtonStyle.font = white;    	 	
    	textButtonStyle.up = skin.getDrawable("blue"); // Add optional skins to button up/down etc  
    	
    	
    	// Window style 'title font' is REQUIRED
    	// Window style background drawable & font colour are OPTIONAL, we can add these later
    	WindowStyle windowStyle = new WindowStyle();
    	windowStyle.titleFont = white;
		
    	initializeSetupWindow(textButtonStyle, windowStyle);  	  
		loadMap();
	}

	public void loadMap() {
		// Load level map
    	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: Loading level map");
		map = new TmxMapLoader().load("maps/level1.tmx");
		Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: Rendering level map");
		renderer = new OrthogonalTiledMapRenderer(map);
	}

	
	// Army Setup Window 
	public void initializeSetupWindow(TextButtonStyle textButtonStyle, WindowStyle windowStyle) {
		
		armySetupWindow = new Window("Army Setup", windowStyle );
		
		// Buttons and Listeners
    	TextButton addSoldierButton = new TextButton("+", textButtonStyle);
    	addSoldierButton.addListener(new ClickListener() {
    		public void clicked(InputEvent event, float x, float y) { 
    			Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: Inc Soldier +1");
    			numSoldiersString = numSoldiersLabel.getText().toString();
    			numSoldiers = Integer.parseInt((String) numSoldiersString) + 1;
    			numSoldiersLabel.setText(Integer.toString(numSoldiers));
    		}
    	});
    	
    	TextButton decreaseSoldierButton = new TextButton("-", textButtonStyle);
    	decreaseSoldierButton.addListener(new ClickListener() {
    		public void clicked(InputEvent event, float x, float y) { 
    			Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: Dec Soldier +1");
    			numSoldiersString = numSoldiersLabel.getText().toString();
    			if(numSoldiers > 0) {
    				numSoldiers = Integer.parseInt((String) numSoldiersString) -1;
    			} else
    				numSoldiers = Integer.parseInt((String) numSoldiersString);
    			numSoldiersLabel.setText(Integer.toString(numSoldiers));
    		}
    	});
    	
    	TextButton startButton = new TextButton("Start Battle", textButtonStyle);
    	
    	startButton.addListener(new ClickListener() {
    		GameController gameController = new GameController();
    		public void clicked(InputEvent event, float x, float y) {
    			Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: Start Battle");
    			numSoldiersString = numSoldiersLabel.getText().toString();
    			gameController.setGuardList(Integer.parseInt((String) numSoldiersString));
	     		gameController.startGame();
	     		armySetupWindow.setVisible(false);
	     		initializeActors(gameController.getGuardList().size());
    		}
    	});
    	
    	TextButton backButton = new TextButton("Back", textButtonStyle);
    	backButton.addListener(new ClickListener() {
    		public void clicked(InputEvent event, float x, float y) {
    			Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: Back");
    			game.setScreen(new MainMenu(game));
    		}
    	});
    	
    	// Labels and Styling
    	LabelStyle labelStyle = new LabelStyle(white, Color.WHITE);
    	
    	soldierTypeLabel = new Label("Guard: ", labelStyle);
    	soldierTypeLabel.setFontScale(0.5f);
    	
    	numSoldiersLabel = new Label(numSoldiersString , labelStyle);
    	numSoldiersLabel.setFontScale(0.5f);  
    	
    	// Add components to setup window
    	// Need to find way to make it 'look' nice....
    	armySetupWindow.padTop(120);
    	armySetupWindow.row();
    	armySetupWindow.add(soldierTypeLabel);
    	armySetupWindow.add(numSoldiersLabel).padRight(20);
    	armySetupWindow.add(addSoldierButton).padBottom(20);
    	armySetupWindow.add(decreaseSoldierButton).padBottom(20).row();
    	armySetupWindow.add(backButton).padBottom(20);
    	armySetupWindow.add(startButton).padBottom(20).align(Align.right);
    	
    	armySetupWindow.setMovable(false);
    	armySetupWindow.pack();
    	armySetupWindow.setBounds(Gdx.graphics.getWidth()/2 - armySetupWindow.getWidth()/2, 
    			Gdx.graphics.getHeight()/2 - armySetupWindow.getHeight()/2, armySetupWindow.getWidth(), armySetupWindow.getHeight());

    	// Add setup widow to stage
    	stage.addActor(armySetupWindow);
	}

	public void initializeActors(int listSize) {
		Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: initializeActors() called");
		group = new Group();
		
		// Initialize texture atlas
		soldierAtlas = new TextureAtlas(Gdx.files.internal("soldier-texpacks/soldier.pack"));
		soldierScaling = 1/6f;
		
		// Getting texture using atlas directly
		
		for(int i=0; i<listSize; i++) {
			AtlasRegion soldierRegion = (soldierAtlas.findRegion("soldier.front0"));
			actor = new Image(soldierRegion);
			actor.setScaling(Scaling.fill);
			actor.setScale(soldierScaling);
			actor.setBounds((Gdx.graphics.getWidth()/3)+(60*i), Gdx.graphics.getHeight()/2, actor.getWidth(), actor.getHeight());
			group.addActor(actor);
			actorArray.add(actor);
		}		
		
		// Getting texture using using a skin
		/*
		for(int i=0; i<listSize; i++) {
			Skin soldierSkin = new Skin(soldierAtlas);
			actor = new Image(soldierSkin.getDrawable("soldier.front0"));
			actor.setScaling(Scaling.fill);
			actor.setScale(soldierScaling);
			actor.setBounds((Gdx.graphics.getWidth()/3)+(60*i), Gdx.graphics.getHeight()/2.5f, actor.getWidth(), actor.getHeight());
			group.addActor(actor);
			actorArray.add(actor);
		}	
		*/
		
		// Add the group of actors to the stage
		stage.addActor(group);
	}	
	
	@Override
	public void dispose() {
		super.dispose();
	}
	
    private void handleInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: A key");
                camera.zoom += 0.02;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.Q)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: Q key");
                camera.zoom -= 0.02;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: LEFT key");
                if (camera.position.x > 0)
                        camera.translate(-3, 0, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: RIGHT key");
                if (camera.position.x < 1024)
                        camera.translate(3, 0, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: DOWN key");
                if (camera.position.y > 0)
                        camera.translate(0, -3, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: UP key");
                if (camera.position.y < 1024)
                        camera.translate(0, 3, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.W)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: W key");
                camera.rotate(-rotationSpeed, 0, 0, 1);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.E)) {
        	Gdx.app.log(MiniKingdomWars.LOG, "LdedLvl: E key");
                camera.rotate(rotationSpeed, 0, 0, 1);
        }
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}


}

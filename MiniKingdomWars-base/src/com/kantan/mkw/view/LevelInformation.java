package com.kantan.mkw.view;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kantan.mkw.application.MiniKingdomWars;

public class LevelInformation extends BaseScreen {
	
	private TextureAtlas textureAtlas;
	private Table table;
	private Skin skin;	
	
	private TextButton setupButton, backButton;	
	
	private Label heading, levelInformation;
	private BitmapFont black, white;   

	
	public LevelInformation(MiniKingdomWars game) {
		super(game);
	}
	
	@Override
    public void render(float delta) {
		//GDClearColour, act() and draw() are called in super.render(delta)
    	super.render(delta);
    }
    
	@Override
    public void resize(int width, int height) {
    	super.resize(width, height);
    	//table.setClip(true);
    	//table.setSize(width, height);
    }
    
	@Override
    public void show() {
    	super.show();
   	
    	textureAtlas = new TextureAtlas("ui/blue.pack");
    	skin = new Skin(textureAtlas);
    	
    	table = new Table(skin);
    	table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    	
    	white = new BitmapFont(Gdx.files.internal("font/white.fnt"), false);

    	TextButtonStyle textButtonStyle = new TextButtonStyle();
    	textButtonStyle.up = skin.getDrawable("blue");
//    	textButtonStyle.down = skin.getDrawable("button.down");
    	textButtonStyle.font = white;
    	
    	setupButton = new TextButton("Prepare Army", textButtonStyle);
    	setupButton.addListener(new ClickListener() {
    		public void clicked(InputEvent event, float x, float y) {
    			//Gdx.app.log(MiniKingdomWars.LOG, "Prepare Army: " + getName());
    			game.setScreen(game.getLoadedLevelScreen());
    		}
    	});
    	
    	backButton = new TextButton("Back", textButtonStyle);
    	backButton.addListener(new ClickListener() {
    		public void clicked(InputEvent event, float x, float y) {
    			Gdx.app.log(MiniKingdomWars.LOG, "LvlInfo: Back");
    			game.setScreen(game.getMainMenuScreen());
    		}
    	});
    	
    	setupButton.pad(10, 70, 10, 70);
    	backButton.pad(10, 70, 10, 70);
    	
    	LabelStyle labelStyle = new LabelStyle(white, Color.WHITE);
    	heading = new Label("Level 1-1", labelStyle);
    	heading.setFontScale(1.5f);
    	
    	levelInformation = new Label("Another king and his feeble army has set foot near your Kingdom. \n" +
    			"This world is only big enough for one Kingdom. Take them out.", labelStyle);
    	levelInformation.setFontScale(0.5f);
    	
    	table.add(heading).padBottom(50).row();    
    	table.add(levelInformation).padBottom(20).row();
    	table.add(setupButton).padBottom(20).row();
    	table.add(backButton).padBottom(20).row();
    	// table.debug();
    	
    	// Fade in Animation
    	table.getColor().a = 0f;
    	table.addAction(fadeIn(0.75f));
    	stage.addActor(table); 
    }
    
	@Override
    public void dispose() {
		super.dispose();
    	textureAtlas.dispose();
    	skin.dispose();
    	white.dispose();
    }
	

}

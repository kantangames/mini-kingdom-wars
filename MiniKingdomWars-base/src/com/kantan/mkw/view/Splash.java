package com.kantan.mkw.view;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kantan.mkw.application.MiniKingdomWars;

public class Splash extends BaseScreen {
	
	private Texture splashTexture;

    public Splash(MiniKingdomWars game) {
    	super(game);
    }
    
    @Override
    public void show() {
    	super.show();
    	
    	//load texture and set filters to linear
    	splashTexture = new Texture(Gdx.files.internal("data/kg-splash.png"));
    	splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
    	TextureRegion splashRegion = new TextureRegion(splashTexture, 0, 0, 1280, 720);
    	Image splashImage = new Image(splashRegion);
    	// set color to 0f (transparent) for fade-in to work correctly
    	splashImage.getColor().a = 0f;
    	
    	// fade in effect
    	splashImage.addAction(sequence(fadeIn(0.75f), delay(1.75f), fadeOut(0.75f), new Action() {
    		public boolean act(float delta) {
    			game.setScreen(game.getMainMenuScreen());
    			return true;
    		}
    	}));
    	stage.addActor(splashImage);

    }
    
    @Override
    public void dispose() {
    	super.dispose();
    	splashTexture.dispose();
    }
}
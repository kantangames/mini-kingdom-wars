package com.kantan.mkw.view;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import com.kantan.mkw.application.MiniKingdomWars;

public class MainMenu extends BaseScreen {	

	private TextureAtlas textureAtlas;
	private Table table;
	private Skin skin;	
	
	private TextButton playButton;
	private TextButton exitButton;	
	private TextButton optionsButton;
	
	private Label heading;
	private BitmapFont black, white;   
	
    public MainMenu(MiniKingdomWars game) {
    	super(game);
    }
    
    @Override
    public void render(float delta) {
    	//GDClearColour, act() and draw() are called in super.render(delta)
    	super.render(delta);
    }
    
    @Override
    public void resize(int width, int height) {
    	super.resize(width, height);;
    	//table.setClip(true);
    	//table.setSize(width, height);
    }
    
    @Override
    public void show() {
    	super.show();
   	
    	textureAtlas = new TextureAtlas("ui/blue.pack");
    	skin = new Skin(textureAtlas);
    	
    	table = new Table(skin);
    	table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    	
    	white = new BitmapFont(Gdx.files.internal("font/white.fnt"), false);
    	black = new BitmapFont(Gdx.files.internal("font/black.fnt"), false);

    	TextButtonStyle textButtonStyle = new TextButtonStyle();
    	textButtonStyle.up = skin.getDrawable("blue");
//    	textButtonStyle.down = skin.getDrawable("button.down");
    	textButtonStyle.font = white;
    	    	
    	playButton = new TextButton("Play", textButtonStyle);
    	playButton.addListener(new ClickListener() {
    		public void clicked(InputEvent event, float x, float y) {
    			Gdx.app.log(MiniKingdomWars.LOG, "MainMenu: Play");
    			game.setScreen(game.getLevelSelectScreen());
    		}
    	});
    	
    	optionsButton = new TextButton("Options", textButtonStyle);
    	exitButton = new TextButton("Exit", textButtonStyle);  
    	exitButton.addListener(new ClickListener() {
    		public void clicked(InputEvent event, float x, float y) {
    			Gdx.app.log(MiniKingdomWars.LOG, "MainMenu: Exit");
    			Gdx.app.exit();
    		}
    	});    	
    	
    	playButton.pad(10, 70, 10, 70);
    	optionsButton.pad(10, 30, 10, 30);
    	exitButton.pad(10, 70, 10, 70);
    	
    	LabelStyle headingStyle = new LabelStyle(white, Color.WHITE);
    	heading = new Label("Mini Kingdom Wars", headingStyle);
    	heading.setFontScale(2);
    	
    	table.add(heading).padBottom(50).row();    
    	table.add(playButton).padBottom(20).row();
    	table.add(optionsButton).padBottom(20).row();
    	table.add(exitButton);
    	
    	// Fade in Animation
    	table.getColor().a = 0f;
    	table.addAction(fadeIn(0.75f));
    	stage.addActor(table); 
    	
//    	table.debug();
    }
    
    @Override
    public void dispose() {
    	super.dispose();
    	textureAtlas.dispose();
    	skin.dispose();
    	white.dispose();
    	black.dispose();
    }
}

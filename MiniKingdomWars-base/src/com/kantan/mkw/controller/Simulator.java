package com.kantan.mkw.controller;

import java.util.Observable;

import com.kantan.mkw.model.Army;
import com.kantan.mkw.model.Level;
import com.kantan.mkw.model.Soldier;
import com.kantan.mkw.model.SoldierAction;
import com.kantan.mkw.model.SoldierType;
import com.kantan.mkw.model.Unit;

public class Simulator extends Observable {

	private Army army;
	private Unit unit;
	private Soldier soldier;
	private SoldierType soldierType;
	private SoldierAction soldierAction;
	
	private Level level;
	
	public Simulator(Level level) {
		this.level = level;		
	}
	
	public Simulator() {
		
	}
	
	public void initializeSimulator() {
		
	}

	public void move() {
		
	}
}

package com.kantan.mkw.controller;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.kantan.mkw.application.MiniKingdomWars;
import com.kantan.mkw.model.Soldier;
import com.kantan.mkw.model.SoldierAction;
import com.kantan.mkw.model.SoldierType;

public class GameController {
	
	private Soldier soldier;
	private Simulator simulator;
	
	ArrayList<Soldier> soldiersList = new ArrayList<Soldier>();
	ArrayList<Soldier> guardList = new ArrayList<Soldier>();
	
	public GameController() {
	}
	
	public void startGame() {
		Gdx.app.log(MiniKingdomWars.LOG, "Gctrl: startGame()");
		getGuardList();
		simulator = new Simulator();
	}	
	
	public void setGuardList(int numGuards) {
		Gdx.app.log(MiniKingdomWars.LOG, "Gctrl: setGuardList()");
		for(int i=0; i<numGuards; i++) {
    		soldier = new Soldier(SoldierType.GUARD, SoldierAction.AWAITORDER);           
    		guardList.add(soldier);
        }
	}
	
	public ArrayList<Soldier> getGuardList() {
		return this.guardList;
	}
	
	public Simulator getSimulator() {
		return this.simulator;
	}
}

	


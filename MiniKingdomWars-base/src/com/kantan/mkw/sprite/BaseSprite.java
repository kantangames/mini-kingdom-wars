package com.kantan.mkw.sprite;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.Json.Serializable;

public class BaseSprite implements Serializable{
	
	private Image sprite;
	private TextureAtlas textureAtlas;
	private TextureRegion textureRegion;	
	
	private float spriteWidth;
	private float spriteHeight;
	private float spriteScale;
	private float spriteLocationX;
	private float spriteLocationY;
	
	public BaseSprite() {
		
	}
	
	// Set sprite values from json
    public void read(Json json, JsonValue jsonMap) {
    	
    	sprite = json.readValue("sprite", Image.class, jsonMap);
    	textureAtlas = json.readValue("textureAtlas", TextureAtlas.class, jsonMap);
    	textureRegion = json.readValue("textureRegion", TextureRegion.class, jsonMap);
    	
    	spriteWidth = json.readValue("spriteWidth", float.class, jsonMap);
    	spriteHeight = json.readValue("spriteHeight", float.class, jsonMap);
    	spriteScale = json.readValue("spriteScale", float.class, jsonMap);
    	spriteLocationX = json.readValue("spriteLocationX", float.class, jsonMap);
    	spriteLocationY = json.readValue("spriteLocationY", float.class, jsonMap);
    	
    }

    // Write to json
    public void write(Json json) {

    	json.writeValue("sprite", sprite);
    	json.writeValue("textureAtlas", textureAtlas);
    	json.writeValue("textureRegion", textureRegion);
    	
    	json.writeValue("spriteWidth", spriteWidth);
    	json.writeValue("spriteHeight", spriteHeight);
    	json.writeValue("spriteScale", spriteScale);
    	json.writeValue("spriteLocationX", spriteLocationX);
    	json.writeValue("spriteLocationY", spriteLocationY);
    }

	
	// sprite getters
	public Image getSpriteImage() {
		return this.sprite;
	}
	
	public float getSpriteScale() {
		return this.spriteScale;
	}
	
	public TextureAtlas getTextureAtlas() {
		return this.textureAtlas;
	}
	
	public TextureRegion getTextureRegion() {
		return this.textureRegion;
	}
	
	public float getSpriteWidth() {
		return this.spriteWidth;
	}
	
	public float getSpriteHeight() {
		return this.spriteHeight;
	}
	
	public float getSpriteLocationX() {
		return this.spriteLocationX;
	}
	
	public float getSpriteLocationY() {
		return this.spriteLocationY;
	}

}

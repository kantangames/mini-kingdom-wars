package com.kantan.mkw.model;

import java.util.Observable;

public class Army extends Observable {

	private int armySize;
	private int totalArmyPower;
	private int totalArmyDefence;
	
	public Army(int armySize, int totalArmyPower, int totalArmyDefence) {
		this.armySize = armySize;
		this.totalArmyPower = totalArmyPower;
		this.totalArmyDefence = totalArmyDefence;
	}
		
	public Army() {
		// TODO Auto-generated constructor stub
	}

	public void setArmySize(int armySize) {
		this.armySize = armySize;
		setChanged();
		notifyObservers();
	}
	
	public void setTotalArmyPower(int totalArmyPower) {
		this.totalArmyPower = totalArmyPower;
		setChanged();
		notifyObservers();
	}
	
	public void setTotalArmyDefence(int totalArmyDefence) {
		this.totalArmyDefence = totalArmyDefence;
		setChanged();
		notifyObservers();
	}
	
	public int getArmySize() {
		return armySize;
	}
	
	public int getTotalArmyPower() {
		return totalArmyPower;
	}
	
	public int getTotalArmyDefence() {
		return totalArmyDefence;
	}	

/*	public static void main(String[]args) {
        Army army = new Army();
        army.setArmySize(1002);
        Soldier soldier;
        ArrayList<Soldier> soldierList = new ArrayList<Soldier>();
     
        for(int i=0; i<=army.getArmySize(); i++) {
    		int size = soldierList.size();
            if(i==501 || i == 503 || i==1002) {
            	soldier = new Soldier(SoldierType.KING, SoldierAction.AWAITORDER);
            } else {
            	soldier = new Soldier(SoldierType.GUARD, SoldierAction.AWAITORDER);
            } 
            
            soldierList.add(soldier);
         }
        
        //soldierList.get(501).getStats();
        
        for(int i=0; i<=soldierList.size()-1; i++) {
        	String king = "King";
        	String comp = soldierList.get(i).getSoldierName();
        	if(king.equals(comp)) {
        		System.out.println("Uppa!");
        	}
        }
	}*/
}


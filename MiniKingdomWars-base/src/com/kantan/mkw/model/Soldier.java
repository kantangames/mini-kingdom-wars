package com.kantan.mkw.model;

import java.util.Observable;

public class Soldier extends Observable {
	
	private int healthPoints;
	private int attackPoints;
	private int defencePoints;
	
	private SoldierType soldierType;
	private SoldierAction soldierAction;
		
	public Soldier(SoldierType soldierType, SoldierAction soldierAction) {
		this.soldierType = soldierType;
		this.soldierAction = soldierAction;
		setBaseAttributes();
	}
		
	public void setBaseAttributes() {
		this.healthPoints = soldierType.getBaseHealthPoints();
		this.attackPoints = soldierType.getBaseAttackPoints();
		this.defencePoints = soldierType.getBaseDefencePoints();
	}
	
	// TEMP DEBUG METHOD (can use this later to display current status of soldier on screen?)
	// Most likely be called getCurrentStatus()
	public void getStats() {
		System.out.println(soldierType.getName());
		System.out.println(healthPoints);
		System.out.println(attackPoints);
		System.out.println(defencePoints);
		System.out.println(soldierAction.getSoldierAction() + "\n");
	}
	
	// Attributes GETTERS
	
	public String getName() {

		return soldierType.getName();
	}	
	
	public int getHealthPoints() {
		
		return this.healthPoints;
	}
	
	public int getAttackPoints() {
		return this.attackPoints;
	}
	
	public int getDefencePoints() {		
		return this.defencePoints;
	}
	
	public String getSoldierAction() {		
		return soldierAction.getSoldierAction();
	}
	
	// Attribute SETTERS
	
	public void setHealthPoints(int healthPoints) {
		this.healthPoints = healthPoints;
		setChanged();
		notifyObservers();
	}
	
	public void setAttackPoints(int attackPoints) {
		this.attackPoints = attackPoints;
		setChanged();
		notifyObservers();
	}
	
	public void setDefencePoints(int defencePoints) {
		this.defencePoints = defencePoints;
		setChanged();
		notifyObservers();
	}
	
	public void setSoldierAction(SoldierAction soldierAction) {
		this.soldierAction = soldierAction;
		setChanged();
		notifyObservers();
	}	
	
}

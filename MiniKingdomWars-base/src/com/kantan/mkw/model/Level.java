package com.kantan.mkw.model;

public class Level {

	private int level = 1;
	private int subLevel = 1;
	
	public Level(int level, int subLevel) {
		this.level = level;
		this.subLevel = subLevel;
	}	
}

package com.kantan.mkw.model;

// Have yet to set up working stats for each type
public enum SoldierType {
	SPEARMAN("Spearman", 30, 12, 4),
	GUARD("Guard", 30, 5, 10),		
	ARCHER("Archer", 30, 8, 8),
	KNIGHT("Knight", 30, 8, 8),
	KINGSMAN("Kingsman", 50, 10, 10),
	KING("King", 100, 8, 5);

	private String name;
	private int baseHealthPoints;
	private int baseAttackPoints;
	private int baseDefencePoints;
	
	private SoldierType(String name, int baseHealthPoints, int baseAttackPoints, int baseDefencePoints) {
		this.name = name;
		this.baseHealthPoints = baseHealthPoints;
		this.baseAttackPoints = baseAttackPoints;
		this.baseDefencePoints = baseDefencePoints;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getBaseHealthPoints() {
		return this.baseHealthPoints;
	}
	
	public int getBaseAttackPoints() {
		return this.baseAttackPoints;
	}
	
	public int getBaseDefencePoints() {
		return this.baseDefencePoints;
	}
}
	
	

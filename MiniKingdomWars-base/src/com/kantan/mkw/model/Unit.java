package com.kantan.mkw.model;

/* I cant create this class fully yet as I don't know what the unit size is until the User Interface passes
 * this information to this class. Then I can pass this information to the Soldier class (i.e. multipliers)
 */

public class Unit {
	
	public enum  DiminishingReturnMultiplier {
		NONE(1.0),
		LITTLE(0.95),
		SMALL(0.90),
		MEDIUM(0.85),
		LARGE(0.80),
		HUGE(0.75);
		
		private double multiplier;
		
		private DiminishingReturnMultiplier(double multiplier) {
			this.multiplier = multiplier;
		}
	}
	
	private int numberOfUnits;
	private int offensiveUnitSize;
	private int defensiveUnitSize;
	private int balancedUnitSize;
	
	private double diminishingMultiplier;
	
	private int offensiveUnitAttack;
	private int defensiveUnitAttack;
	private int balancedUnitAttack;
	
	private int offensiveUnitDefence;
	private int defensiveUnitDefence;
	private int balancedUnitDefence;
	
	// Unit Size GETTERS
	
	public int getNumberOfUnits() {
		return this.numberOfUnits;
	}
	
	public int getOffensiveUnitSize() {
		return this.offensiveUnitSize;
	}
		
	public int getDefensiveUnitSize() {
		return this.defensiveUnitSize;
	}
	
	public int getBalancedUnitSize() {
		return this.balancedUnitSize;
	}
	
	// Unit Attack GETTERS
	
	public int getOffensiveUnitAttack() {
		return offensiveUnitAttack;
	}
	
	public int getDefensiveUnitAttack() {
		return defensiveUnitAttack;
	}
	
	public int getBalancedUnitAttack() {
		return balancedUnitAttack;
	}
	
	// Unit Defence GETTERS
	
	public int getOffensiveUnitDefence() {
		return offensiveUnitDefence;
	}
	
	public int getDefensiveUnitDefence() {
		return defensiveUnitDefence;
	}
	
	public int getBalancedUnitDefence() {
		return balancedUnitDefence;
	}
	
	// Diminishing Multiplier GETTER
	
	public double getDiminishingMultiplier() {
		return diminishingMultiplier;
	}
	
	// Unit Size SETTERS
	
	public void setNumberOfUnits(int number) {
		if(number <= 0) {
			this.numberOfUnits = 1;
		} else if(number > 3) {
			this.numberOfUnits = 3;
		} else {
			this.numberOfUnits = number;
		}
	}
	
	public void setOffensiveUnitSize(int size) {
		this.offensiveUnitSize = size;
	}
	
	public void setDefensiveUnitSize(int size) {
		this.defensiveUnitSize = size;
	}
	
	public void setBalancedUnitSize(int size) {
		this.balancedUnitSize = size;
	}
	
	// Unit Attack SETTERS
	
	public void setOffensiveUnitAttack(int points) {
		this.offensiveUnitAttack = points;
	}
	
	public void setDefensiveUnitAttack(int points) {
		this.defensiveUnitAttack = points;
	}
	
	public void setBalancedUnitAttack(int points) {
		this.balancedUnitAttack = points;
	}
	
	// Unit Defence SETTERS
	
	public void setOffensiveUnitDefence(int points) {
		this.offensiveUnitDefence = points;
	}
	
	public void setDefensiveUnitDefence(int points) {
		this.defensiveUnitDefence = points;
	}
	
	public void setBalancedUnitDefence(int points) {
		this.balancedUnitDefence = points;
	}	
	
	// Diminishing Multiplier SETTER
	
	public void setDiminishingMultiplier(double diminishingMultiplier) {
		this.diminishingMultiplier = diminishingMultiplier;
	}
	
}

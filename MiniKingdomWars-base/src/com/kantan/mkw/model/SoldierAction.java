package com.kantan.mkw.model;

public enum SoldierAction {
	AWAITORDER("Awaiting orders"),
	MOVE("Moving"),
	ATTACK("Attacking"),
	DEFEND("Defending"),
	GUARD("Guarding"),
	RETREAT("Retreating");
	
	private String actionName;
	
	private SoldierAction(String actionName) {
		this.actionName = actionName;
	}
	
	public String getSoldierAction() {
		return this.actionName;
	}
	
}

